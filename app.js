//Importamos los metodos de operaciones
const { generateSuma, generateResta, generateMultiplicacion, generateDivision } = require('./files/operaciones');

console.log('=========================================================');
console.log('          OPERACIONES  ALGEBRAICAS  SIN  YARGS           ');
console.log('=========================================================');

const [,,arg3='base=1']=process.argv;
const [,base] = arg3.split('=');

generateSuma(base)
    // .then(nombreArchivo => console.log(nombreArchivo, 'Creado'))
    // .catch(err => console.log(err));

generateResta(base)
    // .then(nombreArchivo => console.log(nombreArchivo, 'Creado'))
    // .catch(err => console.log(err));

generateMultiplicacion(base)
    // .then(nombreArchivo => console.log(nombreArchivo, 'Creado'))
    // .catch(err => console.log(err));


generateDivision(base)
    // .then(nombreArchivo => console.log(nombreArchivo, 'Creado'))
    // .catch(err => console.log(err));

console.log('Si desea enviar una variable: Ejecute: node app.js --base=[numero]'.black.bgBlue);



