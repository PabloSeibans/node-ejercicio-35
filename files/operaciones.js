const fs = require('fs');
require('colors');

let message = '';

//Validacion de existencia de carpeta
if (!fs.existsSync('salida-operaciones')) {
    fs.mkdirSync('salida-operaciones');
}

const crearSuma = (base = 1) => {

    message = '';

    console.log('=====Suma de', base, '=====');
    //Suma
    for (let i = 1; i <= 100; i++) {
        message += `${base} + ${i} = ${base + i}\n`;
    }

    console.log(message.magenta);

    fs.writeFileSync(`salida-operaciones/Suma-${base}.txt`, message, (err) => {
        if (err) throw err;
        console.log(`Suma - ${base} => Archivo Creado`);
    });

};

const crearResta = (base = 1) => {

    message = '';

    console.log('=====Resta de', base, '=====');

    //Resta
    for (let i = 1; i <= 100; i++) {
        message += `${i} - ${base} = ${i - base}\n`;
    }

    console.log(message.cyan);

    fs.writeFileSync(`salida-operaciones/Resta-${base}.txt`, message, (err) => {
        if (err) throw err;
        console.log(`Resta - ${base} => Archivo Creado`);
    });
};

const crearMultiplicacion = (base = 1) => {
    message = '';
    console.log('=====Multiplicacion de', base, '=====');
    //Multiplicacion

    for (let i = 0; i <= 100; i++) {
        message += `${base} * ${i} = ${base * i}\n`;
    }

    console.log(message.yellow);

    fs.writeFileSync(`salida-operaciones/Multiplicacion-${base}.txt`, message, (err) => {
        if (err) throw err;
        console.log(`Multiplicación - ${base} => Archivo Creado`);
    });
};

const crearDivision = (base = 1) => {

    message = '';

    //Multiplicacion
    for (let i = 0; i <= 100; i++) {
        message += `${i} / ${base} = ${i / base}\n`;
    }

    console.log(message.green);

    fs.writeFileSync(`salida-operaciones/Division-${base}.txt`, message, (err) => {

        if (err) throw err;

        console.log(`Division - ${base} => Archivo Creado`);
        
    });
};


module.exports = {
    generateSuma: crearSuma,
    generateResta: crearResta,
    generateMultiplicacion: crearMultiplicacion,
    generateDivision: crearDivision
}